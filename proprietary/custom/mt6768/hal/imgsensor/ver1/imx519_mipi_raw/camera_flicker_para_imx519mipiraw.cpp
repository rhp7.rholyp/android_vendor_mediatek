//[Sensor]
//name = imx519mipiraw
//bin_ratio = 1,2
//
//[Preview]
//read_freq = 500000000
//pixel_line = 8320
//column_length = 1723
//
//[Capture]
//read_freq = 1388000000
//pixel_line = 12800
//column_length = 3471
//
//[Video]
//read_freq = 1040000000
//pixel_line = 12800
//column_length = 2591
//
//[Video1]
//read_freq = 1000000000
//pixel_line = 5920
//column_length = 701
//
//[Video2]
//read_freq = 500000000
//pixel_line = 5920
//column_length = 701
//
//[Custom1]
//read_freq = 1400000000
//pixel_line = 5920
//column_length = 695
//
//[Custom2]
//read_freq = 1400000000
//pixel_line = 2960
//column_length = 455
//
#define LOG_TAG "imx519mipiraw"

#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>
#include "camera_custom_nvram.h"
#include "camera_custom_sensor.h"
#include "image_sensor.h"
#include "kd_imgsensor_define.h"
#include "camera_AE_PLineTable_imx519mipiraw.h"
#include "camera_info_imx519mipiraw.h"
#include "camera_custom_AEPlinetable.h"
#include "camera_custom_flicker_para.h"
#include <cutils/log.h>


extern "C" fptrFlicker getFlickerParaFunc(MUINT32 sensorType)
{
    if(sensorType==0)
        return NSFeature::RAWSensorInfo<SENSOR_ID>::getFlickerPara;
    else
        return NSFeature::YUVSensorInfo<SENSOR_ID>::getNullFlickerPara;
}

static void get_flicker_para_by_Preview_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1030, 451, 843, -414};
    FLICKER_CUST_STATISTICS EV60_L50 = {1220, 478, 1263, -490};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}

static void get_flicker_para_by_Preview_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1000, 465, 868, -422};
    FLICKER_CUST_STATISTICS EV60_L50 = {1194, 489, 1290, -496};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 31;
    para->flickerSearchRange = 12;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}

static void get_flicker_para_by_Capture_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {70, 80, 90, 100, 120, 130, 140, 160, 170};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1188, 391, 730, -378};
    FLICKER_CUST_STATISTICS EV60_L50 = {1421, 411, 1085, -451};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 24;
    para->flickerSearchRange = 56;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 4;
    para->freq_feature_index[1] = 3;
}

static void get_flicker_para_by_Capture_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {70, 80, 90, 100, 120, 130, 140, 160, 170};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1155, 402, 752, -385};
    FLICKER_CUST_STATISTICS EV60_L50 = {1373, 425, 1123, -460};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 26;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 4;
    para->freq_feature_index[1] = 3;
}

static void get_flicker_para_by_Video_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {70, 80, 90, 100, 120, 130, 140, 160, 170};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1171, 397, 741, -382};
    FLICKER_CUST_STATISTICS EV60_L50 = {1396, 418, 1104, -456};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 24;
    para->flickerSearchRange = 44;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 4;
    para->freq_feature_index[1] = 3;
}

static void get_flicker_para_by_Video_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {70, 80, 90, 100, 120, 130, 140, 160, 170};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1125, 413, 772, -392};
    FLICKER_CUST_STATISTICS EV60_L50 = {1334, 437, 1155, -468};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 28;
    para->flickerSearchRange = 20;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 4;
    para->freq_feature_index[1] = 3;
}

static void get_flicker_para_by_Video1_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {100, 120, 130, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {115, 4022, 7512, -974};
    FLICKER_CUST_STATISTICS EV60_L50 = {271, 2155, 5688, -876};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 24;
    para->flickerSearchRange = 8;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 1;
    para->freq_feature_index[1] = 0;
}

static void get_flicker_para_by_Video1_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {100, 120, 130, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {114, 4077, 7616, -978};
    FLICKER_CUST_STATISTICS EV60_L50 = {248, 2346, 6193, -897};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 4;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 1;
    para->freq_feature_index[1] = 0;
}

static void get_flicker_para_by_Video2_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {70, 100, 120, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {230, 2013, 3761, -797};
    FLICKER_CUST_STATISTICS EV60_L50 = {442, 1321, 3487, -750};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 8;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 2;
    para->freq_feature_index[1] = 1;
}

static void get_flicker_para_by_Video2_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {70, 100, 120, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {228, 2036, 3803, -800};
    FLICKER_CUST_STATISTICS EV60_L50 = {395, 1477, 3898, -779};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 28;
    para->flickerSearchRange = 4;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 2;
    para->freq_feature_index[1] = 1;
}

static void get_flicker_para_by_Custom1_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {100, 120, 130, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {67, 6868, 12828, -1111};
    FLICKER_CUST_STATISTICS EV60_L50 = {116, 4993, 13182, -1091};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 24;
    para->flickerSearchRange = 8;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 1;
    para->freq_feature_index[1] = 0;
}

static void get_flicker_para_by_Custom1_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {100, 120, 130, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {62, 7440, 13897, -1132};
    FLICKER_CUST_STATISTICS EV60_L50 = {106, 5462, 14419, -1114};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 4;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 1;
    para->freq_feature_index[1] = 0;
}

static void get_flicker_para_by_Custom2_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {100, 120, 130, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {6, 71811, 134129, -1712};
    FLICKER_CUST_STATISTICS EV60_L50 = {10, 56411, 148909, -1711};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 24;
    para->flickerSearchRange = 4;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 1;
    para->freq_feature_index[1] = 0;
}

static void get_flicker_para_by_Custom2_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {100, 120, 130, 140, 160, 170, 190, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {5, 80753, 150831, -1742};
    FLICKER_CUST_STATISTICS EV60_L50 = {9, 63476, 167560, -1742};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 0;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 1;
    para->freq_feature_index[1] = 0;
}

// using [Preview_Bin1] parameters for [Custom3_Bin1]
static void get_flicker_para_by_Custom3_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1030, 451, 843, -414};
    FLICKER_CUST_STATISTICS EV60_L50 = {1220, 478, 1263, -490};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}

// using [Preview_Bin1] parameters for [Custom3_Bin2]
static void get_flicker_para_by_Custom3_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1030, 451, 843, -414};
    FLICKER_CUST_STATISTICS EV60_L50 = {1220, 478, 1263, -490};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}

// using [Preview_Bin1] parameters for [Custom4_Bin1]
static void get_flicker_para_by_Custom4_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1030, 451, 843, -414};
    FLICKER_CUST_STATISTICS EV60_L50 = {1220, 478, 1263, -490};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}

// using [Preview_Bin1] parameters for [Custom4_Bin2]
static void get_flicker_para_by_Custom4_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1030, 451, 843, -414};
    FLICKER_CUST_STATISTICS EV60_L50 = {1220, 478, 1263, -490};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}

// using [Preview_Bin1] parameters for [Custom5_Bin1]
static void get_flicker_para_by_Custom5_Bin1(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1030, 451, 843, -414};
    FLICKER_CUST_STATISTICS EV60_L50 = {1220, 478, 1263, -490};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}

// using [Preview_Bin1] parameters for [Custom5_Bin2]
static void get_flicker_para_by_Custom5_Bin2(FLICKER_CUST_PARA* para)
{
    int freq[9] = {80, 90, 100, 120, 130, 140, 170, 210, 230};
    FLICKER_CUST_STATISTICS EV50_L50 = {-194, 4721, 381, -766};
    FLICKER_CUST_STATISTICS EV50_L60 = {1030, 451, 843, -414};
    FLICKER_CUST_STATISTICS EV60_L50 = {1220, 478, 1263, -490};
    FLICKER_CUST_STATISTICS EV60_L60 = {-162, 2898, 247, -642};
    for (int i = 0; i < 9; i++)
        para->flickerFreq[i] = freq[i];
    para->flickerGradThreshold = 25;
    para->flickerSearchRange = 28;
    para->minPastFrames = 3;
    para->maxPastFrames = 14;
    para->EV50_L50 = EV50_L50;
    para->EV50_L60 = EV50_L60;
    para->EV60_L50 = EV60_L50;
    para->EV60_L60 = EV60_L60;
    para->EV50_thresholds[0] = -30;
    para->EV50_thresholds[1] = 12;
    para->EV60_thresholds[0] = -30;
    para->EV60_thresholds[1] = 12;
    para->freq_feature_index[0] = 3;
    para->freq_feature_index[1] = 2;
}


typedef NSFeature::RAWSensorInfo<SENSOR_ID> SensorInfoSingleton_T;
namespace NSFeature {
template <> UINT32 SensorInfoSingleton_T::
impGetFlickerPara(MINT32 sensorMode, MINT32 binRatio, MVOID* const pDataBuf) const
{
    ALOGD("impGetFlickerPara(): mode(%d), binRatio(%d).", sensorMode, binRatio);

    FLICKER_CUST_PARA* para;
    para = (FLICKER_CUST_PARA *)pDataBuf;

    if (sensorMode == e_sensorModePreview && binRatio == 1)
        get_flicker_para_by_Preview_Bin1(para);
    else if (sensorMode == e_sensorModePreview && binRatio == 2)
        get_flicker_para_by_Preview_Bin2(para);
    else if (sensorMode == e_sensorModeCapture && binRatio == 1)
        get_flicker_para_by_Capture_Bin1(para);
    else if (sensorMode == e_sensorModeCapture && binRatio == 2)
        get_flicker_para_by_Capture_Bin2(para);
    else if (sensorMode == e_sensorModeVideo && binRatio == 1)
        get_flicker_para_by_Video_Bin1(para);
    else if (sensorMode == e_sensorModeVideo && binRatio == 2)
        get_flicker_para_by_Video_Bin2(para);
    else if (sensorMode == e_sensorModeVideo1 && binRatio == 1)
        get_flicker_para_by_Video1_Bin1(para);
    else if (sensorMode == e_sensorModeVideo1 && binRatio == 2)
        get_flicker_para_by_Video1_Bin2(para);
    else if (sensorMode == e_sensorModeVideo2 && binRatio == 1)
        get_flicker_para_by_Video2_Bin1(para);
    else if (sensorMode == e_sensorModeVideo2 && binRatio == 2)
        get_flicker_para_by_Video2_Bin2(para);
    else if (sensorMode == e_sensorModeCustom1 && binRatio == 1)
        get_flicker_para_by_Custom1_Bin1(para);
    else if (sensorMode == e_sensorModeCustom1 && binRatio == 2)
        get_flicker_para_by_Custom1_Bin2(para);
    else if (sensorMode == e_sensorModeCustom2 && binRatio == 1)
        get_flicker_para_by_Custom2_Bin1(para);
    else if (sensorMode == e_sensorModeCustom2 && binRatio == 2)
        get_flicker_para_by_Custom2_Bin2(para);
    else if (sensorMode == e_sensorModeCustom3 && binRatio == 1)
        get_flicker_para_by_Custom3_Bin1(para);
    else if (sensorMode == e_sensorModeCustom3 && binRatio == 2)
        get_flicker_para_by_Custom3_Bin2(para);
    else if (sensorMode == e_sensorModeCustom4 && binRatio == 1)
        get_flicker_para_by_Custom4_Bin1(para);
    else if (sensorMode == e_sensorModeCustom4 && binRatio == 2)
        get_flicker_para_by_Custom4_Bin2(para);
    else if (sensorMode == e_sensorModeCustom5 && binRatio == 1)
        get_flicker_para_by_Custom5_Bin1(para);
    else if (sensorMode == e_sensorModeCustom5 && binRatio == 2)
        get_flicker_para_by_Custom5_Bin2(para);

    else {
        ALOGD("impGetFlickerPara(): failed to find prameters.");
        return -1;
    }

    return 0;
}
}
