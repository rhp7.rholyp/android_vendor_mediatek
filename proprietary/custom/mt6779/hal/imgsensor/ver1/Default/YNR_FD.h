
const ISP_NVRAM_YNR_FD_T YNR_FD_%04d = {
    .TBL={.YNR_FD_TBL_EN=0, .YNR_FD_TBL_M_GAIN=10, .YNR_FD_TBL_F_GAIN=10, .YNR_FD_TBL_Y_GAIN=50, .YNR_FD_TBL_U_GAIN=10, .YNR_FD_TBL_V_GAIN=10, .YNR_FD_TBL_Y_Range=48, .YNR_FD_TBL_U_Range=8, .YNR_FD_TBL_V_Range=8},
    .MAP1={.YNR_FD_MAP_EN=1, .YNR_FD_MAP_MAX_GAIN=64, .YNR_FD_MAP_M_GAIN=24, .YNR_FD_MAP_F_GAIN=32, .YNR_FD_MAP_W_R=10, .YNR_FD_MAP_H_R=11, .YNR_FD_MAP_DOWN=9, .YNR_FD_MAP_Y_RANGE=80, .YNR_FD_MAP_U_RANGE=10, .YNR_FD_MAP_V_RANGE=10},
    .MAP2={.rsv1=1, .rsv2=0, .rsv3=20, .rsv4=40, .rsv5=300, .rsv6=200, .rsv7=12, .rsv8=1, .rsv9=28, .rsv10=0, .rsv11=0, .rsv12=0, .rsv13=0, .rsv14=0, .rsv15=0, .rsv16=0, .rsv17=0, .rsv18=0, .rsv19=0, .rsv20=0, .rsv21=0, .rsv22=0, .rsv23=0, .rsv24=0, .rsv25=0, .rsv26=0},
};
