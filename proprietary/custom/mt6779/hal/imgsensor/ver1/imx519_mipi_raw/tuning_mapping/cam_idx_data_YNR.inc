#define IDX_DATA_YNR_DIM_NS    6
#define IDX_DATA_YNR_FACTOR_SZ    3
#define IDX_DATA_YNR_ENTRY_NS    72

static unsigned int _cam_data_entry_YNR_key0000[] = {0X0000000C, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0001[] = {0X00000010, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0002[] = {0X00000020, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0003[] = {0X00000040, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0004[] = {0X00000100, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0005[] = {0X00000000, 0X80300000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0006[] = {0X00000000, 0X80400000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0007[] = {0X0000000C, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0008[] = {0X00000010, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0009[] = {0X00000020, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0010[] = {0X00000040, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0011[] = {0X00000100, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0012[] = {0X00000000, 0X40300000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0013[] = {0X00000000, 0X40400000, 0X00016700, };
static unsigned int _cam_data_entry_YNR_key0014[] = {0X0000000C, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0015[] = {0X00000010, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0016[] = {0X00000020, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0017[] = {0X00000040, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0018[] = {0X00000100, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0019[] = {0X00000000, 0X80300000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0020[] = {0X00000000, 0X80400000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0021[] = {0X0000000C, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0022[] = {0X00000010, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0023[] = {0X00000020, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0024[] = {0X00000040, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0025[] = {0X00000100, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0026[] = {0X00000000, 0X40300000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0027[] = {0X00000000, 0X40400000, 0X0001A700, };
static unsigned int _cam_data_entry_YNR_key0028[] = {0X00000001, 0X40040000, 0X000F7D02, };
static unsigned int _cam_data_entry_YNR_key0029[] = {0X00000001, 0X80040000, 0X00016500, };
static unsigned int _cam_data_entry_YNR_key0030[] = {0X00000001, 0X00040000, 0X000F7D01, };
static unsigned int _cam_data_entry_YNR_key0031[] = {0X00000001, 0X40040000, 0X000FBD02, };
static unsigned int _cam_data_entry_YNR_key0032[] = {0X00000001, 0X80040000, 0X0001A500, };
static unsigned int _cam_data_entry_YNR_key0033[] = {0X00000001, 0X00040000, 0X000FBD01, };
static unsigned int _cam_data_entry_YNR_key0034[] = {0X00000002, 0X40080000, 0X000FFD02, };
static unsigned int _cam_data_entry_YNR_key0035[] = {0X00000002, 0X00080000, 0X000FFD01, };
static unsigned int _cam_data_entry_YNR_key0036[] = {0X00CC0000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_YNR_key0037[] = {0X03300000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_YNR_key0038[] = {0X6C000000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_YNR_key0039[] = {0X90000000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_YNR_key0040[] = {0X00000000, 0XC0000001, 0X000FC7FF, };
static unsigned int _cam_data_entry_YNR_key0041[] = {0X00000000, 0XC0000002, 0X000FC7FF, };
static unsigned int _cam_data_entry_YNR_key0042[] = {0X00000000, 0XC0000008, 0X000FC7FF, };
static unsigned int _cam_data_entry_YNR_key0043[] = {0X00000600, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_YNR_key0044[] = {0X00001800, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_YNR_key0045[] = {0X0001E000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_YNR_key0046[] = {0X0001E00C, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_YNR_key0047[] = {0X00000010, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_YNR_key0048[] = {0X00000020, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_YNR_key0049[] = {0X00000040, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_YNR_key0050[] = {0X00000100, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_YNR_key0051[] = {0X00000000, 0XC0300000, 0X000FDBFF, };
static unsigned int _cam_data_entry_YNR_key0052[] = {0X00000000, 0XC0400000, 0X000FDBFF, };
static unsigned int _cam_data_entry_YNR_key0053[] = {0X00000001, 0X80040000, 0X00015900, };
static unsigned int _cam_data_entry_YNR_key0054[] = {0X00000001, 0X80040000, 0X00019900, };
static unsigned int _cam_data_entry_YNR_key0055[] = {0X0000000C, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0056[] = {0X00000010, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0057[] = {0X00000020, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0058[] = {0X00000040, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0059[] = {0X00000100, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0060[] = {0X00000000, 0X80300000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0061[] = {0X00000000, 0X80400000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0062[] = {0X00000001, 0X80040000, 0X0006FF00, };
static unsigned int _cam_data_entry_YNR_key0063[] = {0XFFFFFFFF, 0XBFFFFFFF, 0X000FFF00, };
static unsigned int _cam_data_entry_YNR_key0064[] = {0X90000010, 0X80024000, 0X000FFF00, };
static unsigned int _cam_data_entry_YNR_key0065[] = {0X00000020, 0X80000001, 0X000FFF00, };
static unsigned int _cam_data_entry_YNR_key0066[] = {0X00000040, 0X80000002, 0X000FFF00, };
static unsigned int _cam_data_entry_YNR_key0067[] = {0X00000100, 0X80000008, 0X000FFF00, };
static unsigned int _cam_data_entry_YNR_key0068[] = {0X00000000, 0X80300000, 0X000FFF00, };
static unsigned int _cam_data_entry_YNR_key0069[] = {0X00000000, 0X80400000, 0X000FFF00, };
static unsigned int _cam_data_entry_YNR_key0070[] = {0XFFFFFFFF, 0X7FFFFFFF, 0X000FFFF8, };
static unsigned int _cam_data_entry_YNR_key0071[] = {0XFFFFFFFF, 0X3FFFFFFF, 0X000FFF07, };

static IDX_MASK_ENTRY _cam_data_entry_YNR[IDX_DATA_YNR_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_YNR_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0001, 8, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0002, 16, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0003, 24, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0004, 32, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0005, 40, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0006, 48, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0007, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0008, 8, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0009, 16, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0010, 24, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0011, 32, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0012, 40, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0013, 48, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0014, 56, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0015, 64, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0016, 72, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0017, 80, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0018, 88, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0019, 40, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0020, 48, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0021, 56, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0022, 64, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0023, 72, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0024, 80, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0025, 88, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0026, 40, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0027, 48, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0028, 96, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0029, 96, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0030, 104, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0031, 96, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0032, 96, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0033, 104, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0034, 112, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0035, 112, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0036, 96, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0037, 112, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0038, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0039, 8, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0040, 16, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0041, 24, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0042, 32, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0043, 96, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0044, 112, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0045, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0046, 0, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0047, 8, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0048, 16, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0049, 24, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0050, 32, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0051, 40, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0052, 48, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0053, 96, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0054, 96, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0055, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0056, 8, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0057, 16, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0058, 24, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0059, 32, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0060, 40, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0061, 48, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0062, 96, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0063, 0, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0064, 8, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0065, 16, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0066, 24, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0067, 32, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0068, 40, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0069, 48, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0070, 96, 25, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_key0071, 112, 26, 0, 0},
};

static unsigned short _cam_data_dims_YNR[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_Zoom,
};

static unsigned short _cam_data_expand_YNR[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_YNR =
{
    {IDX_ALGO_MASK, IDX_DATA_YNR_DIM_NS, (unsigned short*)&_cam_data_dims_YNR, (unsigned short*)&_cam_data_expand_YNR},
    {IDX_DATA_YNR_ENTRY_NS, IDX_DATA_YNR_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_YNR}
};