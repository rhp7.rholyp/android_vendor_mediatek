#define IDX_DATA_SWNR_DIM_NS    6
#define IDX_DATA_SWNR_FACTOR_SZ    3
#define IDX_DATA_SWNR_ENTRY_NS    41

static unsigned int _cam_data_entry_SWNR_key0000[] = {0X0000000C, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_SWNR_key0001[] = {0X000001E0, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_SWNR_key0002[] = {0X00000000, 0X80700000, 0X00016700, };
static unsigned int _cam_data_entry_SWNR_key0003[] = {0X0000000C, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_SWNR_key0004[] = {0X000001E0, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_SWNR_key0005[] = {0X00000000, 0X40700000, 0X00016700, };
static unsigned int _cam_data_entry_SWNR_key0006[] = {0X0000000C, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_SWNR_key0007[] = {0X000001E0, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_SWNR_key0008[] = {0X00000000, 0X80700000, 0X0001A700, };
static unsigned int _cam_data_entry_SWNR_key0009[] = {0X0000000C, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_SWNR_key0010[] = {0X000001E0, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_SWNR_key0011[] = {0X00000000, 0X40700000, 0X0001A700, };
static unsigned int _cam_data_entry_SWNR_key0012[] = {0X00000001, 0X40040000, 0X000F7D02, };
static unsigned int _cam_data_entry_SWNR_key0013[] = {0X00000001, 0X80040000, 0X00016500, };
static unsigned int _cam_data_entry_SWNR_key0014[] = {0X00000001, 0X00040000, 0X000F7D01, };
static unsigned int _cam_data_entry_SWNR_key0015[] = {0X00000001, 0X40040000, 0X000FBD02, };
static unsigned int _cam_data_entry_SWNR_key0016[] = {0X00000001, 0X80040000, 0X0001A500, };
static unsigned int _cam_data_entry_SWNR_key0017[] = {0X00000001, 0X00040000, 0X000FBD01, };
static unsigned int _cam_data_entry_SWNR_key0018[] = {0X00000002, 0X40080000, 0X000FFD02, };
static unsigned int _cam_data_entry_SWNR_key0019[] = {0X00000002, 0X00080000, 0X000FFD01, };
static unsigned int _cam_data_entry_SWNR_key0020[] = {0X00CC0000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_SWNR_key0021[] = {0X03300000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_SWNR_key0022[] = {0X6C000000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_SWNR_key0023[] = {0X00000000, 0XC000000F, 0X000FC7FF, };
static unsigned int _cam_data_entry_SWNR_key0024[] = {0X00000600, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_SWNR_key0025[] = {0X00001800, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_SWNR_key0026[] = {0X0001E000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_SWNR_key0027[] = {0X0001E00C, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_SWNR_key0028[] = {0X000001E0, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_SWNR_key0029[] = {0X00000000, 0XC0700000, 0X000FDBFF, };
static unsigned int _cam_data_entry_SWNR_key0030[] = {0X00000001, 0X80040000, 0X00015900, };
static unsigned int _cam_data_entry_SWNR_key0031[] = {0X00000001, 0X80040000, 0X00019900, };
static unsigned int _cam_data_entry_SWNR_key0032[] = {0X0000000C, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_SWNR_key0033[] = {0X000001E0, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_SWNR_key0034[] = {0X00000000, 0X80700000, 0X0006FF00, };
static unsigned int _cam_data_entry_SWNR_key0035[] = {0X00000001, 0X80040000, 0X0006FF00, };
static unsigned int _cam_data_entry_SWNR_key0036[] = {0XFFFFFFFF, 0XBFFFFFFF, 0X000FFF00, };
static unsigned int _cam_data_entry_SWNR_key0037[] = {0X000001E0, 0X8000000F, 0X000FFF00, };
static unsigned int _cam_data_entry_SWNR_key0038[] = {0X00000000, 0X80700000, 0X000FFF00, };
static unsigned int _cam_data_entry_SWNR_key0039[] = {0XFFFFFFFF, 0X7FFFFFFF, 0X000FFFF8, };
static unsigned int _cam_data_entry_SWNR_key0040[] = {0XFFFFFFFF, 0X3FFFFFFF, 0X000FFF07, };

static IDX_MASK_ENTRY _cam_data_entry_SWNR[IDX_DATA_SWNR_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_SWNR_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0001, 8, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0002, 16, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0003, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0004, 8, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0005, 16, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0006, 0, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0007, 24, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0008, 16, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0009, 0, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0010, 24, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0011, 16, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0012, 0, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0013, 0, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0014, 0, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0015, 0, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0016, 0, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0017, 0, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0018, 0, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0019, 0, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0020, 0, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0021, 0, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0022, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0023, 8, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0024, 0, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0025, 0, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0026, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0027, 0, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0028, 8, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0029, 16, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0030, 0, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0031, 0, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0032, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0033, 8, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0034, 16, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0035, 0, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0036, 0, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0037, 8, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0038, 16, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0039, 0, 25, 0, 0},
    {(unsigned int*)&_cam_data_entry_SWNR_key0040, 0, 26, 0, 0},
};

static unsigned short _cam_data_dims_SWNR[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_Zoom,
};

static unsigned short _cam_data_expand_SWNR[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_SWNR =
{
    {IDX_ALGO_MASK, IDX_DATA_SWNR_DIM_NS, (unsigned short*)&_cam_data_dims_SWNR, (unsigned short*)&_cam_data_expand_SWNR},
    {IDX_DATA_SWNR_ENTRY_NS, IDX_DATA_SWNR_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_SWNR}
};