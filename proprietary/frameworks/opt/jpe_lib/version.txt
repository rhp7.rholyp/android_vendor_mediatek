commit 3df56bf3df7e4fb0060f7e92c7d83384eebff789
Author: Ben Lai <ben.lai@mediatek.com>
Date:   Wed Jan 27 15:15:20 2016 +0800

    [ALPS02548777] [Security][JPE] Remove unnecessary library dependency
    
    [Detail]
    Remove libnativehelper library dependency.
    
    Change-Id: I40df99314a25f9131edb78ee2a67c161d77de5a2
    CR-Id: ALPS02548777
    Feature: Others

commit 8f48ef1294593b73480491ca0df5c12860c10902
Author: sm_admin <sm_admin@mediatek.com>
Date:   Wed Aug 5 01:02:22 2015 +0800

    [ALPS02227924]remove unnecessary PROPRIETARY_LICENSE file
    
    [Detail]
    Remove unnecessary PROPRIETARY_LICENSE file
    [Solution]
    Remove unnecessary PROPRIETARY_LICENSE file
    [Feature]Others
    
    Change-Id: Ia06d162e5d111ba7edbcd844fc9163bfcd6da8ee
    CR-Id: ALPS02227924

commit 8a8ec8c919b005192fd7a01748b845a4c1bb47e6
Author: sm_admin <sm_admin@mediatek.com>
Date:   Tue Aug 4 13:12:11 2015 +0800

    [ALPS02227923]remove unnecessary NOTICE file
    
    [Detail]
    Remove unnecessary NOTICE file
    [Solution]
    Remove unnecessary NOTICE file
    [Feature]Others
    
    Change-Id: I2e80b49908fcc0659053abb5d6be923410ba87f2
    CR-Id: ALPS02227923

commit 401b3f365daf233ba45a4c72203b6526d0ca7f42
Author: srv_sstgitp4 <srv_sstgitp4@mediatek.com>
Date:   Mon Apr 13 12:50:00 2015 +0800

    copy p4 to git

commit 5eb1e36d7027371bbf9cca45993b1b8ac2372b48
Author: GitP4 Service <srv_sstgitp4@mediatek.com>
Date:   Thu Apr 2 19:22:14 2015 +0800

    Initial empty repository
