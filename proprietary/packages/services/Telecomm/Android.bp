// Copyright Statement:
//
// This software/firmware and related documentation ("MediaTek Software") are
// protected under relevant copyright laws. The information contained herein
// is confidential and proprietary to MediaTek Inc. and/or its licensors.
// Without the prior written permission of MediaTek inc. and/or its licensors,
// any reproduction, modification, use or disclosure of MediaTek Software,
// and information contained herein, in whole or in part, shall be strictly prohibited.
//
// MediaTek Inc. (C) 2017. All rights reserved.
//
// BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
// THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
// RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
// AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
// NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
// SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
// SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
// THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
// THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
// CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
// SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
// STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
// CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
// AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
// OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
// MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
//
// The following software/firmware and/or related documentation ("MediaTek Software")
// have been modified by MediaTek Inc. All revisions are subject to any receiver's
// applicable license agreements with MediaTek Inc.

// Build the Telecom service.

bootstrap_go_package {
    name: "soong-Telecom-mediatek",
    pkgPath: "android/soong/Telecom/mediatek",
    deps: [
        "soong-android",
        "soong-java",
    ],
    srcs: [
       "Telecom.go",
    ],
    pluginFor: ["soong_build"],
}

mtk_Telecom_defaults {
     name: "mediatek-Telecom_defaults",
}


android_app {
    name: "MtkTelecom",
    libs: [
        "telephony-common",
        "mediatek-telecom-common",
        "voip-common",
        "mediatek-telephony-base",
        "mediatek-common",
        "mediatek-framework",
    ],
    static_libs: ["com.mediatek.server.telecom.ext"],
    srcs: [
        "src/**/*.java",
        "proto/**/*.proto",
    ],
    resource_dirs: [
        "res",
        "res_ext"
    ],
    proto: {
        type: "nano",
        local_include_dirs: ["proto/"],
        output_params: ["optional_field_style=accessors"],
    },
    overrides: ["Telecom"],
    platform_apis: true,
    certificate: "platform",
    privileged: true,
    optimize: {
        proguard_flags_files: ["proguard.flags"],
    },
    defaults: [
        "SettingsLibDefaults",
        "mediatek-Telecom_defaults",
    ],
}

android_test {
    name: "MtkTelecomUnitTests",
    static_libs: [
        "android-ex-camera2",
        "guava",
        "mockito-target-inline",
        "androidx.test.rules",
        "platform-test-annotations",
        "androidx.legacy_legacy-support-core-ui",
        "androidx.legacy_legacy-support-core-utils",
        "androidx.core_core",
        "androidx.fragment_fragment",
        "com.mediatek.server.telecom.ext",
    ],
    srcs: [
        "tests/src/**/*.java",
        "src/**/*.java",
        "proto/**/*.proto",
    ],
    proto: {
        type: "nano",
        local_include_dirs: ["proto/"],
        output_params: ["optional_field_style=accessors"],
    },
    resource_dirs: [
        "tests/res",
        "res",
        "res_ext",
    ],
    libs: [
        "android.test.mock",
        "android.test.base",
        "android.test.runner",
        "telephony-common",
        "mediatek-common",
        "voip-common",
        "mediatek-telecom-common",
        "mediatek-telephony-base",
        "mediatek-framework",
    ],

    jni_libs: ["libdexmakerjvmtiagent"],

    aaptflags: [
        "--auto-add-overlay",
        "--extra-packages",
        "com.android.server.telecom",
    ],
    manifest: "tests/AndroidManifest.xml",
    optimize: {
        enabled: false,
    },
    platform_apis: true,
    certificate: "platform",
    jacoco: {
        include_filter: ["com.android.server.telecom.*"],
        exclude_filter: ["com.android.server.telecom.tests.*"],
    },
    test_suites: ["device-tests"],
    defaults: ["SettingsLibDefaults"],
}
